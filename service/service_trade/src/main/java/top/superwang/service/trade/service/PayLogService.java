package top.superwang.service.trade.service;

import top.superwang.service.trade.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
public interface PayLogService extends IService<PayLog> {

}
