package top.superwang.service.trade.mapper;

import top.superwang.service.trade.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
public interface OrderMapper extends BaseMapper<Order> {

}
