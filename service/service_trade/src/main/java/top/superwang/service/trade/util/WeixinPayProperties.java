package top.superwang.service.trade.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "weixin.pay")
@Component
public class WeixinPayProperties {

    private String appId;
    private String partner;
    private String partnerKey;
    private String notifyUrl;
}
