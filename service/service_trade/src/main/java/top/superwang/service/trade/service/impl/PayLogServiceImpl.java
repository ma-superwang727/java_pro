package top.superwang.service.trade.service.impl;

import top.superwang.service.trade.entity.PayLog;
import top.superwang.service.trade.mapper.PayLogMapper;
import top.superwang.service.trade.service.PayLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付日志表 服务实现类
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
@Service
public class PayLogServiceImpl extends ServiceImpl<PayLogMapper, PayLog> implements PayLogService {

}
