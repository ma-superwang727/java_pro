package top.superwang.service.trade.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.superwang.service.base.dto.CourseDto;
import top.superwang.service.trade.feign.fallback.EduCourseServiceFallBack;

@Service
@FeignClient(value = "service-edu",fallback = EduCourseServiceFallBack.class)
public interface EduCourseService {
    @GetMapping(value = "/api/edu/course/inner/get-course-info/{courseId}")
    CourseDto getCourseDtoById(@PathVariable(value = "courseId") String courseId);
}
