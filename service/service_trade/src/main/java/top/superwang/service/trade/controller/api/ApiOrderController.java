package top.superwang.service.trade.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import top.superwang.common.base.result.R;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.JwtInfo;
import top.superwang.common.base.util.JwtUtils;
import top.superwang.service.trade.entity.Order;
import top.superwang.service.trade.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
@Api(tags = "订单管理")
//@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/api/trade/order")
public class ApiOrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("根据课程id新增订单")
    @PostMapping("auth/save/{courseId}") // auth 验证登录才能访问
    public R save(@PathVariable String courseId, HttpServletRequest request){
        // 只有登录了,header里面就用token了,就可以获取到用户的信息
        // 根据课程id和用户id来创建订单
        JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
        String orderId = orderService.saveOrder(courseId,jwtInfo.getId());
        return R.ok().data("orderId", orderId);

    }

    @ApiOperation("获取订单")
    @GetMapping("auth/get/{orderId}")
    public R get(@PathVariable String orderId,HttpServletRequest request){
        JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
        Order order = orderService.getByOrderId(orderId,jwtInfo.getId());
        return R.ok().data("item",order);
    }

    @ApiOperation( "判断课程是否购买")
    @GetMapping("auth/is-buy/{courseId}")
    public R GetIsBuyCourse(@PathVariable String courseId,HttpServletRequest request){

        JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
        boolean b = orderService.isBuyByCourseIdAndMid(courseId,jwtInfo.getId());
        return R.ok().data("isBuy", b);

    }


    @ApiOperation( "获取当前用户的所有订单")
    @GetMapping("auth/list")
    public R getUcenterOrderList(HttpServletRequest request){

        JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
        List<Order> orderList = orderService.getUserOrderList(jwtInfo.getId());
        return R.ok().data("items", orderList);

    }


    @ApiOperation( "删除当前用户订单")
    @DeleteMapping("auth/remove/{orderId}")
    public R removeOrder(@PathVariable String orderId,HttpServletRequest request){
        JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
        boolean b = orderService.removeOrder(orderId,jwtInfo.getId());
        if (b){
            return R.ok().message("删除成功");
        }else {
            return R.ok().message("删除失败");
        }

    }


    @ApiOperation("查询订单的支付状态")
    @GetMapping("/query-pay-status/{orderNo}")
    public R queryOrderPayStatus(@PathVariable String orderNo){
        boolean b = orderService.queryPayStatus(orderNo);
        if (b){
            return R.ok().message("支付成功");
        }
        return R.setResult(ResultCode.PAY_RUN);
    }





}

