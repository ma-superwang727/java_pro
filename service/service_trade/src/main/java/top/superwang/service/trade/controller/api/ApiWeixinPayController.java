package top.superwang.service.trade.controller.api;


import com.github.wxpay.sdk.WXPayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.superwang.common.base.result.R;
import top.superwang.common.base.util.StreamUtils;
import top.superwang.service.trade.entity.Order;
import top.superwang.service.trade.service.OrderService;
import top.superwang.service.trade.service.WeixinPayService;
import top.superwang.service.trade.util.WeixinPayProperties;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

//@CrossOrigin
@Slf4j
@Api(tags = "微信支付api")
@RestController
@RequestMapping("/api/trade/weixin-pay")
public class ApiWeixinPayController {

    @Autowired
    private WeixinPayService weixinPayService;

    @Autowired
    private WeixinPayProperties weixinPayProperties;

    @Autowired
    private OrderService orderService;

    @ApiOperation("根据订单号返回map")
    @GetMapping("create-native/{orderNo}")
    public R createNative(@PathVariable String orderNo, HttpServletRequest request){
        Map<String, Object> map = weixinPayService.createNative(orderNo, request.getRemoteAddr());
        return R.ok().data(map);

    }

    // 微信支付回调接口
    @PostMapping("callback/notify")
    public String wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.info("微信支付回调被调用");

        // 获取结果
        ServletInputStream inputStream = request.getInputStream();
        String notifyXml = StreamUtils.inputStream2String(inputStream,"utf-8");

        log.info("获得支付回调xmlString: \n " + notifyXml);

        // 定义响应对象
        HashMap<String, String> returnMap = new HashMap<>();

        // 验证签名,防止伪造
        if (WXPayUtil.isSignatureValid(notifyXml,weixinPayProperties.getPartnerKey())){

            // 解析结果
            Map<String, String> notifyMap = WXPayUtil.xmlToMap(notifyXml);

            // 判断支付是否成功
            //判断支付是否成功
            if("SUCCESS".equals(notifyMap.get("result_code"))){

                // 校验订单金额是否一致
                String totalFee = notifyMap.get("total_fee");
                String outTradeNo = notifyMap.get("out_trade_no");
                Order order = orderService.getOrderByOrderNo(outTradeNo);
                if(order != null && order.getTotalFee().intValue() == Integer.parseInt(totalFee)){

                    // 判断订单状态：保证接口调用的幂等性，如果订单状态已更新直接返回成功响应
                    // 幂等性：无论调用多少次结果都是一样的
                    if(order.getStatus() == 1){
                        returnMap.put("return_code", "SUCCESS");
                        returnMap.put("return_msg", "OK");
                        String returnXml = WXPayUtil.mapToXml(returnMap);
                        response.setContentType("text/xml");
                        log.warn("通知已处理");
                        return returnXml;
                    }else{
                        // 更新订单支付状态，并返回成功响应
                        orderService.updateOrderStatus(notifyMap);
                        returnMap.put("return_code", "SUCCESS");
                        returnMap.put("return_msg", "OK");
                        String returnXml = WXPayUtil.mapToXml(returnMap);
                        response.setContentType("text/xml");
                        log.info("支付成功，通知已处理");
                        return returnXml;
                    }
                }
            }
        }

        // 校验失败，返回失败应答
        returnMap.put("return_code", "FAIL");
        returnMap.put("return_msg", "");
        String returnXml = WXPayUtil.mapToXml(returnMap);
        response.setContentType("text/xml");
        log.warn("微信回调校验失败");
        return returnXml;

    }






}
