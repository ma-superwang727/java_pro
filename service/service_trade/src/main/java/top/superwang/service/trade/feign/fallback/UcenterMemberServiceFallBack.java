package top.superwang.service.trade.feign.fallback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.trade.feign.UcenterMemberService;

@Service
@Slf4j
public class UcenterMemberServiceFallBack implements UcenterMemberService {
    @Override
    public MemberDto getMemberDtoByMemberId(String mId) {
        log.error("调用UCenterMember-熔断保护");
        return null;
    }
}
