package top.superwang.service.trade.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import top.superwang.common.base.result.ResultCode;
import top.superwang.service.base.dto.CourseDto;
import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.trade.entity.Order;
import top.superwang.service.trade.entity.PayLog;
import top.superwang.service.trade.feign.EduCourseService;
import top.superwang.service.trade.feign.UcenterMemberService;
import top.superwang.service.trade.mapper.OrderMapper;
import top.superwang.service.trade.mapper.PayLogMapper;
import top.superwang.service.trade.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.superwang.service.trade.service.PayLogService;
import top.superwang.service.trade.util.OrderNoUtils;

import java.math.BigDecimal;
import java.nio.channels.SelectableChannel;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
@Service
@Slf4j
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduCourseService eduCourseService;

    @Autowired
    private UcenterMemberService ucenterMemberService;

    @Autowired
    private PayLogMapper payLogMapper;

    @Override
    public String saveOrder(String courseId, String memberId) {

        // 查询当前用户是否已经存在订单,如果存在就返回订单id,没有就新建
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        queryWrapper.eq("member_id", memberId);
        Order orderExist = baseMapper.selectOne(queryWrapper);
        log.info("用户是否存在：" + orderExist);
        if (orderExist != null){
            return orderExist.getId(); // 如果存在就返回订单id,
        }

        CourseDto courseDto = eduCourseService.getCourseDtoById(courseId);
        if (courseDto == null){
            log.error("调用远程courseDto为null");
            throw new EduException(ResultCode.ORDER_PARAM_ERROR);
        }

        MemberDto memberDto = ucenterMemberService.getMemberDtoByMemberId(memberId);
        if (memberDto == null){
            log.error("调用远程memberDto为null");
            throw new EduException(ResultCode.ORDER_PARAM_ERROR);
        }

        Order order = new Order();
        order.setOrderNo(OrderNoUtils.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseDto.getTitle());
        order.setCourseCover(courseDto.getCover());
        order.setTeacherName(courseDto.getTeacherName());
        order.setTotalFee(courseDto.getPrice().multiply(new BigDecimal(100)));//分
        order.setMemberId(memberId);
        order.setMobile(memberDto.getMobile());
        order.setNickname(memberDto.getNickname());
        order.setStatus(0);//未支付
        order.setPayType(1);//微信支付
        baseMapper.insert(order);
        log.info("保存成功");
        return order.getId();

    }

    @Override
    public Order getByOrderId(String orderId, String mid) {

        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper .eq("id", orderId).eq("member_id", mid);
        return baseMapper.selectOne(queryWrapper);

    }

    @Override
    public boolean isBuyByCourseIdAndMid(String courseId, String mid) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id", mid)
                .eq("course_id", courseId)
                .eq("status", 1);
        Integer count = baseMapper.selectCount(queryWrapper);
        return count > 0;
    }

    @Override
    public List<Order> getUserOrderList(String mid) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("gmt_create");
        queryWrapper.eq("member_id", mid);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public boolean removeOrder(String orderId, String mid) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",orderId).eq("member_id", mid);
        return this.remove(queryWrapper);
    }

    @Override
    public Order getOrderByOrderNo(String orderNo) {
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("order_no",orderNo);
        return baseMapper.selectOne(orderQueryWrapper);
    }

    @Override
    public void updateOrderStatus(Map<String, String> notifyMap) {

        String orderNo = notifyMap.get("out_trade_no");
        Order order = this.getOrderByOrderNo(orderNo);
        order.setStatus(1); // 支付成功
        baseMapper.updateById(order);

        // 记录支付日志
        PayLog payLog = new PayLog();
        payLog.setOrderNo(orderNo);
        payLog.setPayTime(new Date());
        payLog.setPayType(1);//支付类型
        payLog.setTotalFee(Long.parseLong(notifyMap.get("total_fee")));//总金额(分)
        payLog.setTradeState(notifyMap.get("result_code"));//支付状态
        payLog.setTransactionId(notifyMap.get("transaction_id"));
        payLog.setAttr(new Gson().toJson(notifyMap));
        payLogMapper.insert(payLog);


    }

    @Override
    public boolean queryPayStatus(String orderNo) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_no",orderNo);
        Order order = baseMapper.selectOne(queryWrapper);
        return order.getStatus() == 1;
    }
}
