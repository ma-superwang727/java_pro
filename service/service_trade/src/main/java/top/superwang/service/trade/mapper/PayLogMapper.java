package top.superwang.service.trade.mapper;

import top.superwang.service.trade.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
