package top.superwang.service.trade.service;

import top.superwang.service.trade.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author superWang
 * @since 2021-08-01
 */
public interface OrderService extends IService<Order> {

    String saveOrder(String courseId, String memberId);

    Order getByOrderId(String orderId, String mid);

    boolean isBuyByCourseIdAndMid(String courseId, String mid);

    List<Order> getUserOrderList(String mid);

    boolean removeOrder(String orderId, String mid);

    Order getOrderByOrderNo(String orderNo);

    void updateOrderStatus(Map<String, String> notifyMap);

    boolean queryPayStatus(String orderNo);
}
