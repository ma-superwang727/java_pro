package top.superwang.service.trade.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.trade.feign.fallback.UcenterMemberServiceFallBack;

@Service
@FeignClient(value = "service-ucenter",fallback = UcenterMemberServiceFallBack.class)
public interface UcenterMemberService {

    @GetMapping(value = "/api/ucenter/member/inner/get-member-dto/{mId}")
    MemberDto getMemberDtoByMemberId(@PathVariable(value = "mId") String mId);
}
