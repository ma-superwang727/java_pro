package top.superwang.service.trade.service.impl;

import com.github.wxpay.sdk.WXPayUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.HttpClientUtils;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.trade.entity.Order;
import top.superwang.service.trade.service.OrderService;
import top.superwang.service.trade.service.WeixinPayService;
import top.superwang.service.trade.util.WeixinPayProperties;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class WeixinPayServiceImpl implements WeixinPayService {

    @Autowired
    private OrderService orderService;

    @Autowired
    private WeixinPayProperties weixinPayProperties;


    @Override
    public Map<String, Object> createNative(String orderNo, String remoteAddr) {
        try {


            // 根据课程获取订单
            Order order = orderService.getOrderByOrderNo(orderNo);

            // 调用微信的统一下单api(支付订单)
            HttpClientUtils client = new HttpClientUtils("https://api.mch.weixin.qq.com/pay/unifiedorder");

            //组装接口参数
            Map<String, String> params = new HashMap<>();
            params.put("appid", weixinPayProperties.getAppId());//关联的公众号的appid
            params.put("mch_id", weixinPayProperties.getPartner());//商户号
            params.put("nonce_str", WXPayUtil.generateNonceStr());//生成随机字符串
            params.put("body", order.getCourseTitle()); // 课程名称
            params.put("out_trade_no", orderNo); // 订单号

            // 这个金额参数必须用字符串类型,单位:分
            String totalFree = order.getTotalFee().intValue() + "";
            params.put("total_fee", totalFree);

            params.put("spbill_create_ip", remoteAddr);   // 用户的ip
            params.put("notify_url", weixinPayProperties.getNotifyUrl());  // 自己的回调地址
            params.put("trade_type", "NATIVE"); // 支付类型
            log.info("\n 组装的wx参数: \n" + params);

            // 将上面的map参数组装成 xml格式的字符串,生成带有签名的xml格式字符串
            String xmlParams = WXPayUtil.generateSignedXml(params, weixinPayProperties.getPartnerKey()); // 加上私钥
            log.info("\n 签名后的wx参数: \n" + xmlParams);

            client.setXmlParam(xmlParams); //将参数放入请求对象的方法体
            client.setHttps(true);//使用https形式发送
            client.post();
            String resultXml = client.getContent();//得到响应结果,也是xml格式字符串
            log.info("收到wx响应resultXml: \n" + resultXml);

            Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);

            //错误处理
            if ("FAIL".equals(resultMap.get("return_code")) || "FAIL".equals(resultMap.get("result_code"))) {
                log.error("微信支付统一下单错误(resultMap) - "
                        + "return_code: " + resultMap.get("return_code")
                        + "return_msg: " + resultMap.get("return_msg")
                        + "result_code: " + resultMap.get("result_code")
                        + "err_code: " + resultMap.get("err_code")
                        + "err_code_des: " + resultMap.get("err_code_des"));

                throw new EduException(ResultCode.PAY_WX_UNIFIEDORDER_ERROR);
            }

            // 成功返回支付二维码
            HashMap<String, Object> map = new HashMap<>();
            map.put("result_code", resultMap.get("result_code"));//响应码
            map.put("code_url", resultMap.get("code_url"));//生成二维码的url
            map.put("course_id", order.getCourseId());//课程id
            map.put("total_fee", order.getTotalFee());//订单总金额
            map.put("out_trade_no", orderNo);//订单号

            return map;

        } catch (Exception e) {

            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.PAY_WX_UNIFIEDORDER_ERROR);
        }
    }
}
