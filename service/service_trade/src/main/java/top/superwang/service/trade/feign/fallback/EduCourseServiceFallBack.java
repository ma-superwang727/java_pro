package top.superwang.service.trade.feign.fallback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.superwang.service.base.dto.CourseDto;
import top.superwang.service.trade.feign.EduCourseService;

@Service
@Slf4j
public class EduCourseServiceFallBack implements EduCourseService {
    @Override
    public CourseDto getCourseDtoById(String courseId) {
        log.error("edu-course调用-熔断保护");
        return null;
    }
}
