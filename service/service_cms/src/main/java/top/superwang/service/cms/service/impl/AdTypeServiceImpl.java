package top.superwang.service.cms.service.impl;

import top.superwang.service.cms.entity.AdType;
import top.superwang.service.cms.mapper.AdTypeMapper;
import top.superwang.service.cms.service.AdTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 推荐位 服务实现类
 * </p>
 *
 * @author superWang
 * @since 2021-07-24
 */
@Service
public class AdTypeServiceImpl extends ServiceImpl<AdTypeMapper, AdType> implements AdTypeService {

}
