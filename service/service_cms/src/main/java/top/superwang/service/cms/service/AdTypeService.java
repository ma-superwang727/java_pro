package top.superwang.service.cms.service;

import top.superwang.service.cms.entity.AdType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 推荐位 服务类
 * </p>
 *
 * @author superWang
 * @since 2021-07-24
 */
public interface AdTypeService extends IService<AdType> {

}
