package top.superwang.service.cms.mapper;

import top.superwang.service.cms.entity.AdType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 推荐位 Mapper 接口
 * </p>
 *
 * @author superWang
 * @since 2021-07-24
 */
public interface AdTypeMapper extends BaseMapper<AdType> {

}
