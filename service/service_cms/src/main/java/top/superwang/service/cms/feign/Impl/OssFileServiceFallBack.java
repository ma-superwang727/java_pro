package top.superwang.service.cms.feign.Impl;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.superwang.common.base.result.R;
import top.superwang.service.cms.feign.OssFileService;

@Service
@Slf4j
public class OssFileServiceFallBack implements OssFileService {

    @Override
    public R removeFile(String url) {
        log.info("熔断保护");
        return R.error().message("调用超时");
    }
}