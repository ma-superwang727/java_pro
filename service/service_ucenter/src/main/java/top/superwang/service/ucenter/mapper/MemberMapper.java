package top.superwang.service.ucenter.mapper;

import top.superwang.service.ucenter.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author superWang
 * @since 2021-07-27
 */
public interface MemberMapper extends BaseMapper<Member> {

}
