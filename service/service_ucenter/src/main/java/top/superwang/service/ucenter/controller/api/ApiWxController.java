package top.superwang.service.ucenter.controller.api;


import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.HttpClientUtils;
import top.superwang.common.base.util.JwtInfo;
import top.superwang.common.base.util.JwtUtils;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.ucenter.entity.Member;
import top.superwang.service.ucenter.service.MemberService;
import top.superwang.service.ucenter.util.UcenterProperties;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//@CrossOrigin
@Controller//注意这里没有配置 @RestController ,因为RestController返回json,这里需要返回跳转的url
@RequestMapping("/api/ucenter/wx")
@Slf4j
public class ApiWxController {

    @Autowired
    private UcenterProperties ucenterProperties;

    @Autowired
    private MemberService memberService;

    // 这个方法用来请求微信二维码,返回code参数
    @GetMapping("login")
    public String getQrCode(HttpSession httpSession){

        /*
        * state	否
        * 用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验
        * */
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";

        //处理回调url
        String redirecturi = "";
        try {
            redirecturi = URLEncoder.encode(ucenterProperties.getRedirectUri(), "UTF-8");
            log.info("编码redirecturi成功");
        } catch (UnsupportedEncodingException e) {
            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.WX_EDCODE_URL_ERROR);
        }

        //处理state：生成随机数，存入session
        String state = UUID.randomUUID().toString();
        log.info("生成 state = " + state);
        httpSession.setAttribute("wx_open_state", state);

        String qrcodeUrl = String.format(
                baseUrl,
                ucenterProperties.getAppId(),
                redirecturi,
                state
        );

        return "redirect:" + qrcodeUrl;

    }


    // 回调,获取微信的AccessToken
    @GetMapping("callback8160")
    public String callback(String code,String state,HttpSession session){

        System.out.println("调用了callback");
        System.out.println("code--->" + code);
        System.out.println("state--->" + state);

        // 校验参数
        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(state)){
            log.error("参数为空,非法回调请求");
            throw new EduException(ResultCode.WX_ILLEGAL_CALLBACK_REQUEST_ERROR);
        }

        // 判断session是否一致
        String sessionState = (String)session.getAttribute("wx_open_state");
        if (!sessionState.equals(state)){
            log.error("session不一致,非法回调请求");
            throw new EduException(ResultCode.WX_ILLEGAL_CALLBACK_REQUEST_ERROR);
        }

        // 携带code,appid,appkey,向微信请求token
        String accessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token";
        Map<String, String> accessTokenParam = new HashMap<>();
        accessTokenParam.put("appid", ucenterProperties.getAppId());
        accessTokenParam.put("secret", ucenterProperties.getAppSecret());
        accessTokenParam.put("code", code);
        accessTokenParam.put("grant_type", "authorization_code");
        HttpClientUtils client = new HttpClientUtils(accessTokenUrl,accessTokenParam);

        String resp = "";
        try {
            // 请求
            client.get();
            // 响应
            resp = client.getContent();
            log.info("收到微信返回:" + resp);
        } catch (Exception e) {
            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.WX_FETCH_ACCESSTOKEN_FAILD);
        }

        // 解析返回的json
        Gson gson = new Gson();
        HashMap<String,Object> respMap = gson.fromJson(resp, HashMap.class);

        // 判断微信响应token是否失败
        Object errcodeObj = respMap.get("errcode");
        if(errcodeObj != null){
            String errmsg = (String)respMap.get("errmsg");
            Double errcode = (Double)errcodeObj;
            log.error("获取access_token失败 - " + "message: " + errmsg + ", errcode: " + errcode);
            throw new EduException(ResultCode.WX_FETCH_ACCESSTOKEN_FAILD);
        }

        //微信获取access_token响应成功
        String accessToken = (String)respMap.get("access_token");
        String openid = (String)respMap.get("openid");

        log.info("accessToken = " + accessToken);
        log.info("openid = " + openid);

        //根据access_token获取微信用户的基本信息
        Member member = memberService.selectUserByOpenId(openid);

        if (member == null){

            //向微信的资源服务器发起请求，获取当前用户的用户信息
            String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo";
            Map<String, String> baseUserInfoParam = new HashMap<>();
            baseUserInfoParam.put("access_token", accessToken);
            baseUserInfoParam.put("openid", openid);
            client = new HttpClientUtils(baseUserInfoUrl, baseUserInfoParam);

            String resultUserInfo = null;
            try {
                client.get();
                resultUserInfo = client.getContent();
            } catch (Exception e) {
                log.error(ExceptionUtils.getMessage(e));
                throw new EduException(ResultCode.WX_FETCH_USER_FAILD);
            }

            HashMap<String, Object> resultUserInfoMap = gson.fromJson(resultUserInfo, HashMap.class);
            if(resultUserInfoMap.get("errcode") != null){
                log.error("获取用户信息失败" + "，message：" + resultUserInfoMap.get("errmsg"));
                throw new EduException(ResultCode.WX_FETCH_USER_FAILD);
            }

            // 解析获取微信的用户信息
            String nickname = (String)resultUserInfoMap.get("nickname");
            String headimgurl = (String)resultUserInfoMap.get("headimgurl");
            Double sex = (Double)resultUserInfoMap.get("sex");

            // 说明没有这个用户,执行注册
            member = new Member();
            member.setOpenid(openid);
            member.setNickname(nickname);
            member.setAvatar(headimgurl);
            member.setSex(sex.intValue());
            memberService.save(member);
        }

        // 返回token
        JwtInfo jwtInfo = new JwtInfo();
        jwtInfo.setId(member.getId());
        jwtInfo.setNickname(member.getNickname());
        jwtInfo.setAvatar(member.getAvatar());
        String jwtToken = JwtUtils.getJwtToken(jwtInfo, 1800);

        //携带token跳转
        return "redirect:http://localhost:3000?token=" + jwtToken;
    }


}
