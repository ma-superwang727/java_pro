package top.superwang.service.ucenter.service;

import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.ucenter.entity.Member;
import com.baomidou.mybatisplus.extension.service.IService;
import top.superwang.service.ucenter.entity.Vo.RegisterVo;
import top.superwang.service.ucenter.vo.LoginVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author superWang
 * @since 2021-07-27
 */
public interface MemberService extends IService<Member> {

    void register(RegisterVo registerVo);

    String login(LoginVo loginVo);

    Member selectUserByOpenId(String openId);

    MemberDto innerGetMemberDtoByMId(String mId);
}
