package top.superwang.service.ucenter.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import top.superwang.common.base.result.R;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.JwtInfo;
import top.superwang.common.base.util.JwtUtils;
import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.ucenter.entity.Vo.RegisterVo;
import top.superwang.service.ucenter.service.MemberService;
import top.superwang.service.ucenter.vo.LoginVo;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author superWang
 * @since 2021-07-27
 */
@Api(tags = "用户中心管理")
//@CrossOrigin
@RestController
@RequestMapping("/api/ucenter/member")
@Slf4j
public class ApiMemberController {

    @Autowired
    private MemberService memberService;

    @ApiOperation(value = "用户注册")
    @PostMapping("register")
    public R registerUser(@RequestBody RegisterVo registerVo) {

        memberService.register(registerVo);

        return R.ok().message("注册成功");
    }


    @ApiOperation(value = "登录")
    @PostMapping("login")
    public R loginUser(@RequestBody LoginVo loginVo) {

        String token = memberService.login(loginVo);
        return R.ok().data("token", token).message("登录成功");

    }


    @ApiOperation(value = "根据token获取用户信息")
    @GetMapping("get-login-info")
    public R getLoginInfoByToken(HttpServletRequest request) {
        /*
         * 根据token获取用户的信息,jwt的body根据实际需求来创建.因为这个信息是公开的.
         * 这里用,用户id,名字,头像来被获取
         * */
        try {
            JwtInfo jwtInfo = JwtUtils.getMemberIdByJwtToken(request);
            return R.ok().data("userInfo", jwtInfo);
        } catch (Exception e) {
            log.error("解析用户信息失败");
            throw new EduException(ResultCode.FETCH_USERINFO_ERROR);
        }

    }

    @ApiOperation("根据用户id查询用户信息")
    @GetMapping("inner/get-member-dto/{mId}")
    public MemberDto getMemberDtoByMId(
            @ApiParam(value = "用户id") @PathVariable String mId){

        return memberService.innerGetMemberDtoByMId(mId);
    }





}

