package top.superwang.service.ucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.FormUtils;
import top.superwang.common.base.util.JwtInfo;
import top.superwang.common.base.util.JwtUtils;
import top.superwang.common.base.util.MD5;
import top.superwang.service.base.dto.MemberDto;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.ucenter.entity.Member;
import top.superwang.service.ucenter.entity.Vo.RegisterVo;
import top.superwang.service.ucenter.mapper.MemberMapper;
import top.superwang.service.ucenter.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.superwang.service.ucenter.vo.LoginVo;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author superWang
 * @since 2021-07-27
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void register(RegisterVo registerVo) {

        String code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();
        String nickname = registerVo.getNickname();

        // 是否是空
        if (StringUtils.isEmpty(code)
                || !FormUtils.isMobile(mobile)
                || StringUtils.isEmpty(password)
                || StringUtils.isEmpty(nickname)
                || StringUtils.isEmpty(mobile)){
            throw new EduException(ResultCode.REGISTER_DATA_NULL);

        }

        // 校验手机号
        if (StringUtils.isEmpty(mobile) || !FormUtils.isMobile(mobile)){

            throw new EduException(ResultCode.PHONE_CODE_ERROR);

        }

        // 校验手机验证码
        String checkCode = (String)redisTemplate.opsForValue().get(mobile);
        if (!code.equals(checkCode)){
            throw new EduException(ResultCode.PHONE_CODE_NOT_EQUALS);
        }


        // 校验手机号重复
        QueryWrapper<Member> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if (count > 0){
            throw new EduException(ResultCode.PHONE_REPEAT);
        }

        // 注册
        Member member = new Member();
        member.setNickname(nickname);
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));
        member.setDisabled(false);
        member.setAvatar("https://edu-zaixian-avatar.oss-cn-shanghai.aliyuncs.com/avatar/default_handsome.jpg");
        baseMapper.insert(member);


    }

    @Override
    public String login(LoginVo loginVo) {
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();

        // 校验
        if (StringUtils.isEmpty(mobile)
                || !FormUtils.isMobile(mobile)
                || StringUtils.isEmpty(password)) {
            throw new EduException(ResultCode.PARAMS_ERROR);
        }

        // 手机号不为空
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile",mobile);
        Member member = baseMapper.selectOne(queryWrapper);
        if (member == null){
            throw new EduException(ResultCode.LOGIN_PHONE_ERROR);
        }

        // 密码是否正确
        if (!MD5.encrypt(password).equals(member.getPassword())){
            throw new EduException(ResultCode.LOGIN_PASSWORD_ERROR);
        }

        // 是否被拉黑了
        if (member.getDisabled()){
            throw new EduException(ResultCode.LOGIN_DISABLED_ERROR);
        }


        //登录,返回token
        JwtInfo jwtInfo = new JwtInfo();
        jwtInfo.setId(member.getId());
        jwtInfo.setNickname(member.getNickname());
        jwtInfo.setAvatar(member.getAvatar());
        String jwtToken = JwtUtils.getJwtToken(jwtInfo, 1800);
        return jwtToken;

    }

    @Override
    public Member selectUserByOpenId(String openId) {
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid",openId);
        return baseMapper.selectOne(queryWrapper);


    }

    @Override
    public MemberDto innerGetMemberDtoByMId(String mId) {
        Member member = baseMapper.selectById(mId);
        MemberDto memberDto = new MemberDto();
        BeanUtils.copyProperties(member,memberDto);
        return memberDto;
    }
}
