package top.superwang.service.sms.service;


import com.aliyuncs.exceptions.ClientException;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsService {
    void send(String phone, String foreCode) throws ClientException;
}
