package top.superwang.service.sms.controller;


import com.aliyuncs.exceptions.ClientException;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import top.superwang.common.base.result.R;
import top.superwang.common.base.result.ResultCode;
import top.superwang.common.base.util.FormUtils;
import top.superwang.common.base.util.RandomUtils;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.sms.service.SmsService;

import java.util.concurrent.TimeUnit;

@RequestMapping("/api/sms")
//@CrossOrigin
@RestController
@Api(tags = "发短信")
@Slf4j
public class ApiSmsController {

    @Autowired
    private SmsService smsService;

    @Autowired
    private RedisTemplate redisTemplate;


    @GetMapping("send/{phone}")
    public R sendCode(@PathVariable String phone) throws ClientException {

        // 校验手机号合法
        if (StringUtils.isEmpty(phone) || !FormUtils.isMobile(phone)){
            log.error("手机号不合法");
            throw new EduException(ResultCode.LOGIN_PHONE_ERROR);
        }


        // 校验手机号是否存在
//        boolean b = smsService.phoneIsExis(phone);


        // 生成验证码
        String foreCode = RandomUtils.getFourBitRandom();

        // 发送短信
//        smsService.send(phone,foreCode);

        // 存入redis
        redisTemplate.opsForValue().set(phone,foreCode,5, TimeUnit.MINUTES);

        return R.ok().message("短信发送成功");


    }



}
