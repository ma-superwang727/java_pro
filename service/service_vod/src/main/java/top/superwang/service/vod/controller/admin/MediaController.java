package top.superwang.service.vod.controller.admin;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.superwang.common.base.result.R;
import top.superwang.common.base.result.ResultCode;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.vod.service.VideoService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RestController
//@CrossOrigin
@Api(tags = "阿里云视频管理")
@RequestMapping("/admin/vod/file")
@Slf4j
public class MediaController {

    @Autowired
    private VideoService videoService;

    @PostMapping("upload")
    public R uploadVideo(
            @ApiParam(value = "文件",required = true)
            @RequestParam("file")MultipartFile file
            ){

        try {
            InputStream inputStream = file.getInputStream();
            String originalFilename = file.getOriginalFilename();
            String videoId = videoService.uploadVideo(inputStream, originalFilename);
            return R.ok().message("视频上传成功").data("videoId",videoId);

        } catch (IOException e) {
            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.UPLOAD_ALIYUN_VIDEO_CONTROLLER_ERROR);
        }
    }



    @DeleteMapping("remove/{vodId}")
    public R removeVideo(
            @ApiParam(value="阿里云视频id", required = true)
            @PathVariable String vodId){

        try {
            videoService.removeVideoById(vodId);
            return R.ok().message("视频删除成功");
        } catch (Exception e) {
            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.VIDEO_DELETE_ALIYUN_ERROR);
        }
    }


//     批量删除视频
    @DeleteMapping("removeBatch")
    public R removeBatchByVideoIdList(
            @ApiParam(value="视频id列表", required = true)
            @RequestBody List<String> list){

        try {
            videoService.removeVideosByVideoIdList(list);
            return R.ok().message("批量删除视频成功");
        } catch (Exception e) {
            log.error(ExceptionUtils.getMessage(e));
            throw new EduException(ResultCode.VIDEO_DELETE_ALIYUN_ERROR);
        }
    }








    // 测试接口,可以忽略
    @DeleteMapping("remove1/{vodId}")
    public R removeVideo1(
            @ApiParam(value="阿里云视频id", required = true)
            @PathVariable String vodId){

            log.info("收到调用了1" + vodId);
            return R.ok().message("视频删除成功");

    }



}
