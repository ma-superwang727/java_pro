package top.superwang.service.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.superwang.common.base.result.ResultCode;
import top.superwang.service.base.exception.EduException;
import top.superwang.service.vod.service.VideoService;
import top.superwang.service.vod.util.AliyunVodSDKUtils;
import top.superwang.service.vod.util.VodProperties;

import java.io.InputStream;
import java.util.List;


@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VodProperties vodProperties;


    @Override
    public String uploadVideo(InputStream inputStream, String originalFilename) {

        // 视频名字
        String title = originalFilename.substring(0, originalFilename.indexOf("."));


        UploadStreamRequest request = new UploadStreamRequest(
                vodProperties.getKeyid(),
                vodProperties.getKeysecret(),
                title,originalFilename,inputStream);

        /* 模板组ID(可选) */
//        request.setTemplateGroupId(vodProperties.getTemplateGroupId());
        /* 工作流ID(可选) */
//        request.setWorkflowId(vodProperties.getWorkflowId());


        UploadVideoImpl uploadVideo = new UploadVideoImpl();

        UploadStreamResponse response = uploadVideo.uploadStream(request);

        String videoId = response.getVideoId();


        if (StringUtils.isEmpty(videoId)){
            log.error("阿里云上传失败" + response.getCode() + "-" + response.getMessage());
            throw new EduException(ResultCode.UPLOAD_ALIYUN_VIDEO_ERROR);
        }


        return videoId;
    }

    @Override
    public void removeVideoById(String videoId) throws ClientException {
        DefaultAcsClient client = AliyunVodSDKUtils.initVodClient(
                vodProperties.getKeyid(),
                vodProperties.getKeysecret());

        DeleteVideoRequest request = new DeleteVideoRequest();
        request.setVideoIds(videoId);
        client.getAcsResponse(request);
    }



    // 批量删除接口,阿里云规定不能超过20个
    @Override
    public void removeVideosByVideoIdList(List<String> list) throws ClientException {
        DefaultAcsClient client = AliyunVodSDKUtils.initVodClient(
                vodProperties.getKeyid(),
                vodProperties.getKeysecret());


        int size = list.size();

        StringBuffer stringBuffer = new StringBuffer();
        DeleteVideoRequest request = new DeleteVideoRequest();
        log.info("列表:" + list);


        for (int i = 0; i < size; i++) {
            stringBuffer.append(list.get(i));

            if (i == size -1 || i % 20 == 19){
                request.setVideoIds(stringBuffer.toString());
                log.info("需要删除的videoId字符串:" + stringBuffer.toString()); // 形式:"xxxxx,xxxxx,xxxxx,xxxxx"
                client.getAcsResponse(request);
                stringBuffer = new StringBuffer();
            }else if (i % 20 < 19){
                stringBuffer.append(",");
            }

        }

    }

    @Override
    public String getPlayAuth(String videoSourceId) throws ClientException {
//初始化client对象
        DefaultAcsClient client = AliyunVodSDKUtils.initVodClient(
                vodProperties.getKeyid(),
                vodProperties.getKeysecret());

        //创建请求对象
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId(videoSourceId);

        //获取响应
        GetVideoPlayAuthResponse response = client.getAcsResponse(request);

        return response.getPlayAuth();



    }


}
