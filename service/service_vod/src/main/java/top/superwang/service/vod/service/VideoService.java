package top.superwang.service.vod.service;

import com.aliyuncs.exceptions.ClientException;

import java.io.InputStream;
import java.util.List;

public interface VideoService {

    String uploadVideo(InputStream inputStream,String originalFilename);

    void removeVideoById(String videoId) throws ClientException;

    void removeVideosByVideoIdList(List<String> list) throws ClientException;


    String getPlayAuth(String videoSourceId) throws ClientException;
}
