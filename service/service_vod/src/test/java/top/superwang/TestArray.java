package top.superwang;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestArray {



    @Test
    public void testAddArrayList(){
        ArrayList<String> str = new ArrayList<>();

        str.add("a");
        str.add("b");
        str.add("c");
        str.add("d");
        str.add("e");
        str.add("f");
        str.add("g");
        addData(str);

    }


    // list的数量不超过4个，超过4个需要；另外打包
    public static void addData(List<String> list){
        ArrayList<String> res = new ArrayList<>();

        int size = list.size();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < size; i++) {
            sb.append(list.get(i));
            if (i == size -1 || i%4==3){
                // 删
                System.out.println(sb.toString());
                sb = new StringBuffer();
            }else if (i % 4 < 3){
                sb.append(",");
            }
        }
        System.out.println(sb);
    }

}
