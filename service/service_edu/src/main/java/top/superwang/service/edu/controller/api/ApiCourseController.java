package top.superwang.service.edu.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.superwang.common.base.result.R;
import top.superwang.service.base.dto.CourseDto;
import top.superwang.service.edu.entity.Course;
import top.superwang.service.edu.entity.vo.ChapterVo;
import top.superwang.service.edu.entity.vo.WebCourseQueryVo;
import top.superwang.service.edu.entity.vo.WebCourseVo;
import top.superwang.service.edu.service.ChapterService;
import top.superwang.service.edu.service.CourseService;

import java.util.List;


//@CrossOrigin// 防止跨域问题
@Api(tags = "课程")
@RestController
@RequestMapping("/api/edu/course")
@Slf4j
public class ApiCourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private ChapterService chapterService;


    // 返回统一的格式
    @ApiOperation("根据条件查询课程信息")
    @GetMapping("list")
    public R list(@ApiParam(value = "查询对象",required = true) WebCourseQueryVo webCourseQueryVo){
        List<Course> courses = courseService.webSelectList(webCourseQueryVo);

        return R.ok().data("courseList",courses);
    }


    @ApiOperation("根据课程id查询详细信息")
    @GetMapping("get/{courseId}")
    public R getCourseInfo(
            @ApiParam(value = "课程id",required = true)
            @PathVariable String courseId){

        WebCourseVo webCourseVo = courseService.selectWebCourseVoById(courseId);

        List<ChapterVo> chapterVoList = chapterService.getChapterNestedList(courseId);

        return R.ok().data("course", webCourseVo).data("chapterVoList", chapterVoList);

    }


    // 这个是服务之间的调用,所以不用返回给R对象
    @ApiOperation("根据课程id查询课程信息")
    @GetMapping("inner/get-course-info/{courseId}")
    public CourseDto getCourseDtoByCourseId(
            @ApiParam(value = "课程id",required = true) @PathVariable String courseId){

        CourseDto courseDto = courseService.innerGetCourseDtoByCourseId(courseId);
        System.out.println(courseDto);
        return courseDto;
    }


}
