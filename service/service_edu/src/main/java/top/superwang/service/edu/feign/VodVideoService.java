package top.superwang.service.edu.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import top.superwang.common.base.result.R;
import top.superwang.service.edu.feign.fallback.VodVideoServiceFallback;

import java.util.List;

@Service
@FeignClient(value = "service-vod" , fallback = VodVideoServiceFallback.class) // fallback备胎
public interface VodVideoService {

    @DeleteMapping("/admin/vod/file/remove/{vodId}")
    R removeVideoById(@PathVariable("vodId") String vodId); // 这里的pathVariable参数要写上,因为是远程调用


    // 测试方法,可忽略
    @DeleteMapping("/admin/vod/file/remove1/{vodId}")
    R removeVideoById1(@PathVariable("vodId") String vodId); // 这里的pathVariable参数要写上,因为是远程调用


    @DeleteMapping("/admin/vod/file/removeBatch")
    R removeVideosByList(@RequestBody List<String> list);


}
