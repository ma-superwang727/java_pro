package top.superwang.service.edu.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.superwang.common.base.result.R;
import top.superwang.service.edu.entity.Course;
import top.superwang.service.edu.entity.vo.SubjectVo;
import top.superwang.service.edu.entity.vo.WebCourseQueryVo;
import top.superwang.service.edu.service.CourseService;
import top.superwang.service.edu.service.SubjectService;

import java.util.List;

//@CrossOrigin// 防止跨域问题
@Api(tags = "课程分类")
@RestController
@RequestMapping("/api/edu/subject")
@Slf4j
public class ApiSubjectController {


    @Autowired
    private SubjectService subjectService;


    // 返回统一的格式
    @ApiOperation("嵌套的课程分类")
    @GetMapping("nested-list")
    public R nestList(){
        List<SubjectVo> subjectVos = subjectService.nestedSubjectData();

        return R.ok().data("items",subjectVos);
    }


}
