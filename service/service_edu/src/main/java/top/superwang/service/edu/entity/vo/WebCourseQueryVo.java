package top.superwang.service.edu.entity.vo;

import lombok.Data;

import java.io.Serializable;

// 按 课程的类别，销量，最新，价格排序查询
@Data
public class WebCourseQueryVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String subjectParentId;
    private String subjectId;
    private String buyCountSort;   // 销量
    private String gmtCreateSort;  // 最新
    private String priceSort;  // 价格

    private Integer type; //价格正序：1，价格倒序：2
}