package top.superwang.service.edu.controller.api;

/*
*
* 首页的课程数据和讲师数据
*
* */

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.superwang.common.base.result.R;
import top.superwang.service.edu.entity.Course;
import top.superwang.service.edu.entity.Teacher;
import top.superwang.service.edu.service.CourseService;
import top.superwang.service.edu.service.TeacherService;

import java.util.List;

@Api(tags = "首页课程和讲师数据")
//@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/api/edu/index")
public class ApiIndexController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private CourseService courseService;

    @ApiOperation(value = "首页数据")
    @GetMapping()
    public R getIndexTeacherAndCourse(){

        // 展示4个热门讲师上首页
        List<Teacher> teachers =  teacherService.getIndexTeacherInfo();

        // 展示8个热门课程上首页
        List<Course> courses = courseService.getIndexCourseInfo();

        return R.ok().data("courseList",courses).data("teacherList",teachers);


    }

}
