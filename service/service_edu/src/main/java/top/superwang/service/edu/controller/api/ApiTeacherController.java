package top.superwang.service.edu.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.superwang.common.base.result.R;
import top.superwang.service.edu.entity.Teacher;
import top.superwang.service.edu.service.TeacherService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author wangw
 * @since 2021-04-18
 */
//@CrossOrigin// 防止跨域问题
@Api(tags = "讲师")
@RestController
@RequestMapping("/api/edu/teacher")
@Slf4j
public class ApiTeacherController {

    @Autowired
    private TeacherService teacherService;


    // 返回统一的格式
    @ApiOperation("获取所有教师列表")
    @GetMapping("list")
    public R listAll(){
        List<Teacher> list = teacherService.list(null);
        return R.ok().data("datalist",list);

    }

    @ApiOperation(value = "根据id查讲师信息和课程")
    @GetMapping("get/{id}")
    public R findTeacherByName(@ApiParam(value = "讲师id",readOnly = true) @PathVariable String id){

        Map<String, Object> stringObjectMap = teacherService.selectTeacherAndCourseByTeacherId(id);

        return R.ok().data(stringObjectMap);

    }





}

