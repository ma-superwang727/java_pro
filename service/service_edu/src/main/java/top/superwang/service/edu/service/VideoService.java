package top.superwang.service.edu.service;

import org.springframework.stereotype.Repository;
import top.superwang.service.edu.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author wangw
 * @since 2021-04-18
 */
@Repository
public interface VideoService extends IService<Video> {


    void removeAliyunVideoById(String id);


    void removeAliyunVideoById1(String id);

    // 通过章节id批量删除
    void removeAliyunVideosByChapterId(String chapterId);

    // 通过课程id批量删除
    void removeAliyunVideosByCourseId(String courseId);

}
