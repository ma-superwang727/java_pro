package top.superwang.service.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import top.superwang.service.edu.entity.Video;
import top.superwang.service.edu.feign.VodVideoService;
import top.superwang.service.edu.mapper.VideoMapper;
import top.superwang.service.edu.service.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author wangw
 * @since 2021-04-18
 */
@Service
@Slf4j
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

    @Autowired
    private VodVideoService vodVideoService;

    @Override
    public void removeAliyunVideoById(String id) {

        log.warn("根据videoid删除视频:" + id);
        Video video = baseMapper.selectById(id);

        String videoSourceId = video.getVideoSourceId();
        log.warn("根据videoid查到视频的id:" + videoSourceId);

        vodVideoService.removeVideoById(videoSourceId);
    }

    @Override
    public void removeAliyunVideoById1(String id) {
        log.info("edu-service调用远程" + id);
        vodVideoService.removeVideoById1(id);
    }

    @Override
    public void removeAliyunVideosByChapterId(String chapterId) {

        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.select("video_source_id");
        videoQueryWrapper.eq("chapter_id",chapterId);

        List<Map<String, Object>> maps = baseMapper.selectMaps(videoQueryWrapper);
//        [{video_source_id=99014fe663424c61bdef59185c4688ce}, {video_source_id=47e1357607e54561b3be4728db20607d}]
        log.info("所有章节视频:" + maps.toString());

        List<String> videoIdList = new ArrayList<>();

        for (Map<String, Object> map : maps) {
            String video_source_id = (String)map.get("video_source_id");
            videoIdList.add(video_source_id);
        }

        log.info("章节视频,组装的videoId列表:" + videoIdList); //[99014fe663424c61bdef59185c4688ce, 47e1357607e54561b3be4728db20607d]
        vodVideoService.removeVideosByList(videoIdList);

    }

    @Override
    public void removeAliyunVideosByCourseId(String courseId) {
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();

        videoQueryWrapper.select("video_source_id");
        videoQueryWrapper.eq("course_id",courseId);

        List<Map<String, Object>> maps = baseMapper.selectMaps(videoQueryWrapper);
        List<String> videoIdList = new ArrayList<>();

        for (Map<String, Object> map : maps) {
            String video_source_id = (String)map.get("video_source_id");
            videoIdList.add(video_source_id);
        }
        vodVideoService.removeVideosByList(videoIdList);

    }


}
