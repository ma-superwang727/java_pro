package top.superwang.service.edu.feign.fallback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.superwang.common.base.result.R;
import top.superwang.service.edu.feign.VodVideoService;

import java.util.List;

@Service
@Slf4j
public class VodVideoServiceFallback implements VodVideoService {
    @Override
    public R removeVideoById(String vodId) {
        log.error("视频的熔断保护!要删除的视频id:" + vodId);
        return R.error();
    }

    @Override
    public R removeVideoById1(String vodId) {
        log.error("测试测试视频的熔断保护!要删除的视频id:" + vodId);
        return R.error();
    }

    @Override
    public R removeVideosByList(List<String> list) {
        log.error("批量删除视频的熔断保护!要删除的视频id列表:" + list);
        return R.error();
    }
}
