package top.superwang.service.edu.controller;


import org.springframework.web.bind.annotation.*;
import top.superwang.common.base.result.R;

//@CrossOrigin
@RestController
@RequestMapping("/user")
public class TeacherLogin {

//    登录
    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }

//    获取用户信息
    @GetMapping("info")
    public R userInfo(){

        return R.ok().data("name","admin")
                .data("roles","[admin,admin]")
                .data("avatar","https://himg.bdimg.com/sys/portrait/item/pp.1.95e41477.jydnhbMJqN3upwldmnVgbg.jpg?tt=1628254277548");
    }


    @PostMapping("logout")
    public R logout(){
        return R.ok();
    }

}
