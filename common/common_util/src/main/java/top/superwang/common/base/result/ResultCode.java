package top.superwang.common.base.result;


import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum ResultCode {


    SUCCESS(true, 20000, "成功"),

    ERROR(false, 20001, "失败"),

    UPLOAD_ERROR(false, 21004, "文件上传失败"),

    UPLOAD_EXCEL_ERROR(false, 21006, "上传excel文件失败"),

    UPLOAD_ALIYUN_VIDEO_ERROR(false, 21010, "阿里云上传失败"),
    UPLOAD_ALIYUN_VIDEO_CONTROLLER_ERROR(false, 21011, "controller层上传视频失败"),
    VIDEO_DELETE_ALIYUN_ERROR(false, 21012, "删除视频失败"),
    FAILED_GET_PLAY_AUTH(false, 21013, "获取视频播放凭证失败"),

    LOGIN_PHONE_ERROR(false, 22000, "手机号码不合法"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(false, 22001, "短信发送过于频繁"),
    SMS_SEND_ERROR(false, 22002, "短信发送失败"),

    // 用户管理
    PHONE_CODE_ERROR(false, 23001, "手机号码错误"),
    REGISTER_DATA_NULL(false, 23002, "注册数据错误"),
    PHONE_CODE_NOT_EQUALS(false, 23003, "验证码错误"),
    PHONE_REPEAT(false, 23004, "手机号重复"),
    PARAMS_ERROR(false, 23005, "参数错误"),

    LOGIN_MOBILE_ERROR(false, 23006, "手机号或密码有误"),
    LOGIN_PASSWORD_ERROR(false, 23007, "手机号或密码有误"),
    LOGIN_DISABLED_ERROR(false, 23008, "禁止登录"),
    FETCH_USERINFO_ERROR(false, 23009, "解析用户失败"),

    WX_EDCODE_URL_ERROR(false,24001,"编码请求二维码失败"),
    WX_ILLEGAL_CALLBACK_REQUEST_ERROR(false,24002,"非法回调请求"),
    WX_FETCH_ACCESSTOKEN_FAILD(false,24003,"请求微信token失败"),
    WX_FETCH_USER_FAILD(false,24004,"获取微信用户信息失败"),


    ORDER_PARAM_ERROR(false,25001,"订单参数有误"),

    PAY_WX_UNIFIEDORDER_ERROR(false,26001,"微信支付未知错误"),
    PAY_RUN(false,26002,"支付中")

    ;


    private Boolean success;

    private Integer code;

    private String message;


    ResultCode(Boolean success, Integer code, String message) {

        this.success = success;
        this.code = code;
        this.message = message;
    }
}
